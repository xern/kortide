DATA_LIMIT = 512 * 1024

randomString = (len) ->
  chars = '0123456789ABCDEF'
  result = ''
  for i in [len..1] then result += chars[Math.round(Math.random() * (chars.length - 1))]
  result

exports.index = (req, res) ->
  len = req.params.length || 32
  ms = req.params.ms || 0
  len = DATA_LIMIT if len > DATA_LIMIT
  setTimeout (()->res.send randomString len), ms
