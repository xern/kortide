cluster = require 'cluster'
http = require 'http'
httpProxy = require 'http-proxy'
numCPUs = require('os').cpus().length

express = require "express"
routes = require "./routes/index"
http = require "http"
path = require "path"

argv = require('optimist').argv

SERVER_PORT = if argv.p then 3001 else 3000
PROXY_PORT = 3000

removeHeaders = (req, res, next) ->
    res.removeHeader("X-Powered-By");
    res.removeHeader("ETag");
    next()

app = express()
app.configure ->
  app.set "port", SERVER_PORT
  app.use removeHeaders
  app.use app.router
app.get "/:length/:ms?", routes.index

if cluster.isMaster
  numOfApiServers = if argv.n then argv.n else numCPUs

  for i in [0..Math.abs(numOfApiServers - 1)] then cluster.fork()
  cluster.on 'exit', (worker, code, signal) -> console.log 'worker ' + worker.process.pid + ' died'

  if argv.p
    console.log "Proxy server listening on port " + PROXY_PORT
    httpProxy.createServer(SERVER_PORT, 'localhost').listen(PROXY_PORT);
else
  server = http.createServer app
  server.on "connection", (socket) -> socket.setNoDelay true
  server.listen app.get("port"), -> console.log "Data server listening on port " + app.get("port")
